package handler

import (
	"encoding/json"
	"fmt"
	"net/http"
	"os"

	"gitlab.com/vlpasnj33/weather-svc/internal/core/domain/openweather"
)

func WeatherHandler(w http.ResponseWriter, r *http.Request) {
	city := r.URL.Query().Get("city")

	apiURL := fmt.Sprintf("https://api.openweathermap.org/data/2.5/weather?q=%s&appid=%s&lang=%s&units=%s", 
						   	city, os.Getenv("WEATHER_API_KEY"), "uk", "metric")

	resp, err := http.Get(apiURL)
	if err != nil {
		http.Error(w, "Failed to make request", http.StatusInternalServerError)
		return
	}
	defer resp.Body.Close()

	if resp.StatusCode != http.StatusOK {
		http.Error(w, "Failed to fetch data from OpenWeatherMap API", http.StatusInternalServerError)
		return
	}

	var weatherData domain.WeatherData
	if err := json.NewDecoder(resp.Body).Decode(&weatherData); err != nil {
		http.Error(w, "Failed to decode JSON", http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(weatherData)
}
