module gitlab.com/vlpasnj33/weather-svc

go 1.22.0

require github.com/joho/godotenv v1.5.1

require github.com/rs/cors v1.10.1 // indirect
