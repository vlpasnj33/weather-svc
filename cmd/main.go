package main

import (
	"context"
	"log"
	"net/http"
	"os"
	"os/signal"
	"time"

	"github.com/rs/cors"

	"github.com/joho/godotenv"
	"gitlab.com/vlpasnj33/weather-svc/internal/adapters/handler"
)

func main() {
	err := godotenv.Load()
	if err != nil {
		log.Fatal("Error loading .env file")
	}

	mux := http.NewServeMux()

	c := cors.Default()

	h := c.Handler(mux)

	mux.HandleFunc("GET /weather", handler.WeatherHandler)

	server := &http.Server{
		Addr:    ":8080",
		Handler: h,
	}

	go func() {
		if err := server.ListenAndServe(); err != nil {
			log.Fatal("Error")
		}
	}()

	// Graceful Shutdown
	stop := make(chan os.Signal, 1)
	signal.Notify(stop, os.Interrupt)

	<-stop
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	if err := server.Shutdown(ctx); err != nil {
		log.Fatal("Error")
	}
}
